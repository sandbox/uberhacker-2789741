<?php
/**
 * @file
 * Override Drush commands with the Composer equivalent, if applicable.
 *
 * @author Ed Reel (https://www.drupal.org/u/uberhacker)
 */

/**
 * Implements hook_drush_command_alter().
 */
function druposer_drush_command_alter(&$command) {
  // Check if a valid Drupal project exists.
  if (druposer_check()) {
    $native_command = $command['command'];
    $valid_commands = array(
      'pm-download',
      'pm-uninstall',
      'pm-update',
      'pm-updatecode',
    );
    // Only operate on valid override commands.
    if (in_array($native_command, $valid_commands)) {
      // Make sure a Drupal project argument has been provided.
      if (!isset($command['arguments'][0])) {
        drush_set_error('DRUSH_APPLICATION_ERROR', dt('The Drupal project is missing.'));
        exit;
      }
      $project = 'drupal/' . drupal_to_semver($command['arguments'][0]);
      switch ($native_command) {
        // Override pm-download with "composer require $project".
        case 'pm-download':
          // Make sure the composer project can be installed, if missing.
          if (strpos($project, 'drupal/composer') === FALSE) {
            $args = array(
              'require',
              $project,
            );
            drush_druposer($args);
          }
          break;
        // Override pm-uninstall with "composer remove $project".
        case 'pm-uninstall':
          $args = array(
            'remove',
            $project,
          );
          drush_druposer($args);
          break;
        // Override pm-update with "composer update $project".
        // Also execute database updates after the update.
        case 'pm-update':
          $args = array(
            'update',
            $project,
          );
          drush_druposer($args, true);
          break;
        // Override pm-update with "composer update $project".
        case 'pm-updatecode':
          $args = array(
            'update',
            $project,
          );
          drush_druposer($args);
          break;
      }
    }
  }
}

/**
 * Check if a valid Drupal project exists.
 *
 * @return bool
 *   True if composer.json is a valid Drupal project configuration, false otherwise.
 */
function druposer_check() {
  if (!$composer = druposer_config()) {
    return false;
  }
  if (!isset($composer->replace->{'drupal/core'})) {
    return false;
  }
  if (!isset($composer->repositories->drupal)) {
    $drupal_major_version = drush_drupal_major_version();
    $args = array(
      'config',
      'repositories.drupal',
      'composer',
      'https://packages.drupal.org/' . $drupal_major_version,
    );
    drush_druposer($args);
  }
  return true;
}

/**
 * Get the Composer configuration.
 *
 * @return object
 *   Composer configuration object, or false otherwise.
 */
function druposer_config() {
  static $composer = null;
  if (!isset($composer)) {
    $composer_json = DRUPAL_ROOT . '/composer.json';
    if (!file_exists($composer_json)) {
      return false;
    }
    $composer = json_decode(file_get_contents($composer_json));
  }
  return $composer;
}

/**
 * Convert Drupal-style version to semantic version.
 *
 * @param string $ver
 *   The Drupal project and version.
 *
 * @return string
 *   The semantic version of the Drupal project.
 */
function drupal_to_semver($ver = '') {
  $semver = array();
  $project_semver = '';
  $composer = druposer_config();
  $drupal_major_version = drush_drupal_major_version();
  $packages_url = 'https://packages.drupal.org/' . $drupal_major_version;
  if ($composer->repositories->drupal->url == $packages_url) {
    $parts = explode('-', $ver);
    foreach ($parts as $p => $part) {
      if ($p == 0) {
        $project = $part;
      } elseif ($part != $drupal_major_version . '.x') {
        $semver[] = $part;
      }
    }
    $token = '-';
    $alt_project_semver = $project;
  }
  else {
    $pieces = array();
    $parts = explode('-', $ver);
    foreach ($parts as $p => $part) {
      if ($p == 0) {
        $project = $part;
      }
      else {
        $pieces[] = explode('.', $part);
      }
    }
    foreach ($pieces as $piece) {
      if ($piece != 'x') {
        $semver[] = $piece;
      }
      if ($piece == 'dev') {
        $semver = array('dev-master');
        break;
      }
    }
    $token = '.';
    $alt_project_semver = $project . ':' . $drupal_major_version . '.*';
  }
  if (!empty($semver)) {
    $project_semver = $project . ':' . implode($token, $semver);
  }
  return $project_semver ? $project_semver : $alt_project_semver;
}

/**
 * Drush Composer; Invokes a Composer command.
 *
 * @param array $args
 *   An array of Composer command arguments we are to execute.
 * @param bool $updatedb
 *   If true, run the database updates after code updates.
 */
function drush_druposer($args, $updatedb = false) {
  $drupal_major_version = drush_drupal_major_version();
  // Make sure the PHP requirements are met.
  $php = '5.3.2';
  $current = phpversion();
  if (version_compare($current, $php, '<')) {
    drush_set_error('DRUSH_APPLICATION_ERROR', dt('Composer requires at least PHP !php in order to run properly. You are currently on PHP !current.', array(
      '!php' => $php,
      '!current' => $current,
    )));
    exit;
  }

  // Check if the composer project is installed.
  $composer_drush = getenv('HOME') . '/.drush/composer/composer.drush.run.inc';
  if (!file_exists($composer_drush)) {
    drush_set_error('DRUSH_APPLICATION_ERROR', dt('The druposer project depends on the composer project.  Install with "drush dl composer && drush cc drush".'));
    exit;
  }

  // Include the run in a different file so that PHP versions < 5.3 do not fail
  // parsing the file.
  require_once($composer_drush);

  // Change to the Drupal root directory.
  chdir(DRUPAL_ROOT);

  // Disable the project before removing it on Drupal 7.
  if ($args[0] == 'remove' && $drupal_major_version == 7) {
    drush_invoke_process('@self', 'pm-disable', array($args[1], '-y'));
  }

  // Run the Composer command using the given function arguments.
  drush_composer_run($args);

  // Make sure the database updates are executed.
  if ($updatedb) {
    drush_invoke_process('@self', 'updatedb', array('-y'));
  }
  exit;
}
