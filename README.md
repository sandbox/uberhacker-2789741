# Druposer

The Drush Decomposer - Override Drush commands with the [Composer](http://getcomposer.org) equivalent, if applicable.


## Dependencies:
```
1. [Drupal](https://www.drupal.org/project/drupal)
2. [Composer](https://getcomposer.org/download/)
3. [Drush](http://www.drush.org/en/master/install/)
4. [Composer for Drush](https://www.drupal.org/project/composer)
```


## Installation:
```
$ cd ~/.drush
$ git clone --branch 8.x-1.x uberhacker@git.drupal.org:sandbox/uberhacker/2789741.git druposer
$ drush cc drush
```


## Usage:
```
$ drush pm-download | pm-uninstall | pm-update | pm-updatecode <project-version>
```


## Examples:
To download the lastest token project:
```
$ cd /path/to/drupal/project/directory
$ drush rl token
 Project  Release         Date         Status
 token    8.x-1.x-dev     2016-Aug-23  Development
 token    8.x-1.0-beta1   2016-Aug-10  Supported, Recommended
 token    8.x-1.0-alpha2  2016-Jan-21
 token    8.x-1.0-alpha1  2015-Dec-24

$ drush pm-download token-8.x-1.x-beta1
- or -
$ drush dl token-1.x-beta1
```
To uninstall the ctools project:
```
$ cd /path/to/drupal/project/directory
$ drush pml --package="Chaos tool suite"
 Name                              Type    Status         Version
 Chaos tools (ctools)              Module  Enabled        8.x-3.0-alpha27
 Chaos tools Views (ctools_views)  Module  Not installed  8.x-3.0-alpha27

$ drush pm-uninstall ctools-8.x-3.0-alpha27
- or -
$ drush pmu ctools
```
To update the metatags project:
```
$ cd /path/to/drupal/project/directory
$ drush rl metatag
 Project  Release         Date         Status
 metatag  8.x-1.0-beta10  2016-Aug-22  Supported, Recommended
 metatag  8.x-1.x-dev     2016-Aug-22  Development, Installed

$ drush pm-update metatag-8.x-1.0-beta10
- or -
$ drush up metatag
```
